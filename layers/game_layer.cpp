#include "game_layer.h"
#include "core/engine.h"

#include "events/event_handler.h"
#include "events/input_events.h"

#include "core/profiling/profiler.h"
#include "thirdparty/imgui/imgui.h"

#include <cstdlib>
#include <ctime>

#define PILLAR_DISTANCE 1500

// for key codes
#include <GLFW/glfw3.h>

GameLayer::GameLayer(const std::string& name) :
    Layer(name),
    player(nullptr),
    background(nullptr),
    ground(nullptr),
    xVel(1),
    playerVelocity(xVel, 0)
{
    cam = make_ref<Camera2D>(WIDTH, HEIGHT);
    scene = make_ref<Scene2D>(cam);

    EventHandler::get().listen(EventType::KeyPress, BIND_EVENT_FUNC(GameLayer::on_event));

    srand((unsigned)time(NULL));
    nextPillarPos = PILLAR_DISTANCE;
}

GameLayer::~GameLayer()
{

}

void GameLayer::on_bind()
{
    glm::vec2 pos(0.f, 0.f);
    glm::vec2 dims(64, 64);
    player = make_ref<Sprite>(pos, dims);
    player->set_texture("assets/textures/grumpy_static.png");
    player->set_z_order(-1.f);

    glm::vec2 backgroundDims(1280, 720);
    background = make_ref<Sprite>(pos, backgroundDims);
    background->set_texture("assets/textures/background.png");
    background->set_z_order(0.5f);

    ground = make_ref<SpriteBatch>();
    ground->set_texture("assets/textures/pipe.png");
    // Generate the first two pillars
    generate_pillars();
    generate_pillars();
}

void GameLayer::on_unbind()
{
    cam.unref();
    scene.unref();
}

void GameLayer::on_update(float dt)
{
    PROFILE_FUNCTION();
    if (!pause)
    {
        // Check if the player and ground are colliding
        if (ground->sprite_overlapping(player) || abs(player->get_pos().y) > 600)
        {
            lost = true;
        }
        else
        {
            // Apply gravity to the player
            playerVelocity += glm::vec2(0.f, -9.8) * dt * speedFactor;
        }
        player->move(playerVelocity * dt);
        cam->set_pos(glm::vec2(player->get_pos().x, 0));
        background->set_pos(glm::vec2(player->get_pos().x, 0));

        // Create new pillars every PILLAR_DISTANCE meters
        if (player->get_pos().x >= nextPillarPos - PILLAR_DISTANCE)
        {
            if (!lost) score++;
            generate_pillars();
        }
    }

    //update_ui();
    scene->draw_sprite(background);
    scene->draw_sprite_batch(ground);
    scene->draw_sprite(player);
}

void GameLayer::update_ui()
{
    // Store the flags for a static window
    ImGuiWindowFlags flags = 0;
    flags |= ImGuiWindowFlags_NoMove;
    flags |= ImGuiWindowFlags_NoResize;
    flags |= ImGuiWindowFlags_NoCollapse;
    flags |= ImGuiWindowFlags_NoDecoration;
    flags |= ImGuiWindowFlags_AlwaysAutoResize;
    flags |= ImGuiWindowFlags_NoSavedSettings;
    flags |= ImGuiWindowFlags_NoNav;
    flags |= ImGuiWindowFlags_NoFocusOnAppearing;

    // Fps window
    {
        ImGui::SetNextWindowPos(ImVec2(5, 5), ImGuiCond_Once);
        ImGui::SetNextWindowBgAlpha(0.35f);

        ImGui::Begin("FPS", nullptr, flags);

        ImGui::Text("%.1f FPS (%.3f ms/frame)", ImGui::GetIO().Framerate, 1000.0f / ImGui::GetIO().Framerate);
        ImGui::End();
    }

    // Score text
    {
        ImGui::SetNextWindowPos(ImVec2(ImGui::GetIO().DisplaySize.x * 0.5, 5), ImGuiCond_Once, ImVec2(0.5f, 0));
        ImGui::SetNextWindowBgAlpha(0.35f);

        ImGui::Begin("Score", nullptr, flags);

        ImGui::Text("Score: %d", score);
        ImGui::End();
    }

    if (lost)
    {
        ImGui::SetNextWindowPos(ImVec2(ImGui::GetIO().DisplaySize.x * 0.5, ImGui::GetIO().DisplaySize.y * 0.5), ImGuiCond_Once, ImVec2(0.5f, 0.5f));

        // TODO: display an image here instead of text
        ImGui::Begin("Lose", nullptr, flags);
        ImGui::Text("YOU LOST");
        ImGui::End();
    }
}

void GameLayer::on_fixed_update()
{
    
}

void GameLayer::on_event(Ref<Event> e)
{
    if (e->get_event_type() == EventType::KeyPress)
    {
        Ref<KeyboardPressEvent> event = (Ref<KeyboardPressEvent>)e;
        if (event->code == GLFW_KEY_ESCAPE) 
            Engine::get().quit();
        else if (event->code == GLFW_KEY_SPACE)
        {
            pause = false;
            if (!lost) playerVelocity = glm::vec2(xVel, 3.f);
        }
        else if (event->code == GLFW_KEY_R)
            reset();
    }

}

void GameLayer::reset()
{
    lost = false;
    score = 0;
    player->set_pos({0.f, 0.f});
    background->set_pos({0.f, 0.f});
    cam->set_pos({0, 0});
    playerVelocity = glm::vec2(xVel, 0);
    pause = true;
    // reset the ground
    nextPillarPos = 0;
    ground->clear_instances();
    generate_pillars();
    generate_pillars();
}

void GameLayer::generate_pillars()
{
    nextPillarPos += PILLAR_DISTANCE;

    float randomShift = rand() % 300;

    // Create bottom pillar
    glm::vec2 bottomPos(nextPillarPos, -900 + randomShift);
    glm::vec2 bottomDims(130, 500);
    glm::vec3 groundColor = glm::vec3(1.f);
    ground->create_instance(bottomPos, bottomDims, groundColor);


    // Create top pillar
    glm::vec2 topPos(nextPillarPos, 900 + randomShift);
    glm::vec2 topDim(130, 500);
    ground->create_instance(topPos, topDim, groundColor);
}