#ifndef GAME_LAYER_H
#define GAME_LAYER_H

#include "core/layer.h"
#include "scene/2d/scene2d.h"
#include "scene/2d/camera2d.h"

class GameLayer : public Layer
{
public:
    GameLayer(const std::string& name = "Game Layer");
    virtual ~GameLayer();

    virtual void on_bind() override;
    virtual void on_unbind() override;

    virtual void on_update(float dt) override;
    virtual void on_fixed_update() override;
    virtual void on_event(Ref<Event> e) override;
private:
    void update_ui();
    void reset();
    void generate_pillars();

    Ref<Scene2D> scene;
    Ref<Camera2D> cam;

    Ref<Sprite> player;
    Ref<Sprite> background;
    Ref<SpriteBatch> ground;

    bool pause = true;
    bool lost = false;
    int score = 0;
    glm::vec2 playerVelocity;
    float speedFactor = 0.001;
    double nextPillarPos;
    const float xVel;
};

#endif // GAME_LAYER_H