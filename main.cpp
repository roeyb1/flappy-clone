#include "core/engine.h"
#include "core/profiling/profiler.h"
#include "layers/game_layer.h"
#include "scene/ui/imgui_layer.h"

int main()
{
    Profiler::get().begin_session("MTEngine");
    Engine::get().initialize();
    
    Ref<GameLayer> gameLayer = make_ref<GameLayer>();
    Engine::get().push_layer(gameLayer);


    Engine::get().run();

    Profiler::get().end_session();
    return 0;
}