#version 330 core

in vec2 texCoord;
in vec3 color;

uniform sampler2D aTexture;

out vec4 FragColor;

void main()
{
       vec4 texColor = texture(aTexture, texCoord) * vec4(color, 1.0);
       if (texColor.a < 0.1)
              discard;
       FragColor = texColor;
}
