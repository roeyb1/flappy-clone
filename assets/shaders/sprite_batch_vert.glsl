#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec2 aTexCoord;
// This actually takes up locations 2-5
layout (location = 2) in mat4 instTrans;
layout (location = 6) in vec3 aColor;

out vec2 texCoord;
out vec3 color;

uniform mat4 view_project;

void main()
{
       gl_Position = view_project * instTrans * vec4(aPos, 1.0);
       texCoord = aTexCoord;
       color = aColor;
}
